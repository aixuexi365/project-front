import request from '@/utils/request'
const prefix = "/thirdThirdInterface";

// 查询菜单列表
export function listAppInfo(query) {
  return request({
    url: prefix + '/pagelist',
    method: 'post',
    data: query
  })
}

// 应用状态修改
export function changeAppInfoStatus(id, status) {
  const data = {
    id,
    status
  }
  return request({
    url: prefix + '/update',
    method: 'post',
    data: data
  })
}

// 应用修改
export function updateAppInfoById(data) {
  return request({
    url: prefix + '/update',
    method: 'post',
    data: data
  })
}

// 应用修改
export function addAppInfo(data) {
  return request({
    url: prefix + '/add',
    method: 'post',
    data: data
  })
}


// 查询应用详细
export function getAppInfoById(id) {
  return request({
    url: prefix + '/info/' + id,
    method: 'get'
  })
}

// 删除应用
export function batchDeleteThirdInfo(data) {
  return request({
    url: prefix + '/deleteInfo',
    method: 'post',
    data: data
  })
}
