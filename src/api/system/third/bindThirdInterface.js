import request from '@/utils/request'
const prefix = "/thirdThirdResourceRel";

// 查询菜单列表
export function listBindInfo(query) {
  return request({
    url: prefix + '/pagelist',
    method: 'post',
    data: query
  })
}

// 应用状态修改
export function bindRel(data) {
  return request({
    url: prefix + '/bind',
    method: 'post',
    data: data
  })
}

// 应用修改
export function cancelAuthRelAll(data) {
  return request({
    url: prefix + '/deleteByIds',
    method: 'post',
    data: data
  })
}



// 应用修改
export function addAppInfo(data) {
  return request({
    url: prefix + '/add',
    method: 'post',
    data: data
  })
}


// 查询应用详细
export function getAppInfoById(id) {
  return request({
    url: prefix + '/info/' + id,
    method: 'get'
  })
}

// 删除应用
export function batchDeleteThirdInfo(data) {
  return request({
    url: prefix + '/deleteInfo',
    method: 'post',
    data: data
  })
}
